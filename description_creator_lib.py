import xml.etree.ElementTree as XML
import file_paths
import random

class DescriptionCreator:

    def __init__(self):
        self.ability_template_tree = XML.parse(file_paths.get_description_file_template_path())
        self.ability_template_tree.getroot().find("Name").text = "game\\" + "abilities"

    def save_everything_to_file(self):
        self.ability_template_tree.write(file_paths.get_text_file_export("abilities"))

    def add_ability_text(self, descript):
        new_entry = self._create_entry(descript)
        self.ability_template_tree.getroot().find("Entries").append(new_entry)

        # A "next entry id" has never been written
        if (self.ability_template_tree.getroot().find("NextEntryID").text == "None"):
            self.ability_template_tree.getroot().find("NextEntryID").text = new_entry.find("ID").text

        return int(new_entry.find("ID").text)

    def _create_entry(self, text):
        new_entry = XML.Element("Entry")
        new_entry.append(XML.Element("ID"))
        new_entry.append(XML.Element("DefaultText"))
        new_entry.append(XML.Element("FemaleText"))

        new_entry.find("ID").text = str(random.randint(10000, 99999))
        new_entry.find("DefaultText").text = text

        return new_entry