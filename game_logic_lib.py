# Contains rules based on game logic rather than plain json
# For example: which classes have "Spell" for ability type id?

def get_usage_type(class_name):
    caster_classes = ["priest", "druid", "wizard"]
    accrual_classes = ["chanter", "cipher"]

    for caster in caster_classes:
        if caster in class_name.lower():
            return "Spell"

    for accrual in accrual_classes:
        if accrual in class_name.lower():
            return "ClassAccruedResource"
    
    return "ClassPowerPool"