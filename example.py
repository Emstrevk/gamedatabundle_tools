#!/bin/python3

# This script is an example of modifying the ability progression table for a subclass
# It creates and inserts two new abilities and disables an old one

import parsing_lib
import file_paths
from description_creator_lib import DescriptionCreator

# Make sure you have the needed folders before attempting any exports 
file_paths.ensure_dirs()

# Initialize a description creator to keep track of any new texts we create
# Ingame text modding has its own little system so we need these stored separately
dc = DescriptionCreator()

# Fetch and export the class progression table
# Basically: finds the PT data in game files, exports a copy with a new id that we can modify
pt_priest_file = parsing_lib.export_progression_table("Priest")

# The "difficult" way to get abilities; using id found using manual search earlier
#druid_firebrand_id = "5e9e0a16-4633-4408-9218-d44c65be0040"
#abilities_source_file = file_paths.get("abilities")

# Loose matching; there's only one class called "Priest" :)
priest_class = parsing_lib.get_class_object("priest")

# Create two new abilities by searching for best match by name and ensuring they are castable by priests
firebrand_ability = parsing_lib.lazy_get_ability_object("firebrand")
firebrand_ability = parsing_lib.change_ability_to_class(firebrand_ability, priest_class)

breath_of_flames_ability = parsing_lib.lazy_get_ability_object("breath_of_flame")
breath_of_flames_ability = parsing_lib.change_ability_to_class(breath_of_flames_ability, priest_class)

bof_title_id = dc.add_ability_text("Firebreathing")
bof_desc_id = dc.add_ability_text("Breathe fire for the glory of Sata-- Magran!")
breath_of_flames_ability = parsing_lib.assign_description_to_object(breath_of_flames_ability, bof_desc_id)
breath_of_flames_ability = parsing_lib.assign_name_to_object(breath_of_flames_ability, bof_title_id)

# Export the new ability objects to mod files, saving their paths
firebrand_priest_file = parsing_lib._export_object(firebrand_ability)
breath_of_flames_file = parsing_lib._export_object(breath_of_flames_ability)

# Read the newly exported ability files, add their data to exported priest PT file
parsing_lib._add_to_progression_table(pt_priest_file, firebrand_priest_file)
parsing_lib._add_to_progression_table(pt_priest_file, breath_of_flames_file)

magran_subclass = parsing_lib.get_subclass_object("priest", "magran")

# Enable the new spells for only the magran subclass
parsing_lib.enable_in_pt_for_subclass(pt_priest_file, firebrand_ability["DebugName"], magran_subclass["ID"], require_if_none=True)
parsing_lib.enable_in_pt_for_subclass(pt_priest_file, breath_of_flames_ability["DebugName"], magran_subclass["ID"], require_if_none=True)

# Remove "Holy Radiance" (or any matches for "radiance" for that matter) for magran subclass (hey! balance!)
parsing_lib.disable_in_pt_for_subclass(pt_priest_file, "radiance", magran_subclass["ID"])

# This creates the description files for your mod
# If you skip this step you will have a bunch of "MISSING STRING" kind of texts in game
dc.save_everything_to_file()