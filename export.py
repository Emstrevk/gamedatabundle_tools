import file_paths
import parsing_lib
import sys
import json

file_name = sys.argv[1]
id_string = sys.argv[2]

if file_name is "" or id_string is "":
    print("Need two args...")
else:
    file_path = file_paths.get(file_name)

    # Called "hits" because legacy but getbyid only fetches one...
    hits = parsing_lib._get_by_id(file_path, id_string)
    parsing_lib._export_object(hits)
        
            