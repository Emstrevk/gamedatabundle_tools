import file_paths
import parsing_lib
import sys

file_name = sys.argv[1]
object_name = sys.argv[2]

if file_name is "" or object_name is "":
    print("Need two args...")
else:
    file_path = file_paths.get(file_name)

    hits = parsing_lib.find(file_path, object_name)

    for hit in hits:
        print(hit.get("DebugName") + ": " + hit.get("ID") + " (" + hit.get("$type") + ")")
