# Pillars of Eternity II: Deadfire Gamedatabundle Parser

This project represents an effort to provide an easy-to-use and automated method
	to perform common gamedatabundle modifications.

Code was originally indended only for personal use but I decided to share it
	in case it saves someone else the effort. Don't expect military grade code or documentation :)

Prerequisites: Python 3, experience level: "I dabble"

See example.py for an example mod generating script built on top the parsing "lib".

I'd suggest playing around with the example to try and create your own changes.

Check file_paths.py for location settings (e.g. from where to read game data sources). 
	The default settings expect you to perform your git clone in the Deadfire root folder (where your game.exe is). 