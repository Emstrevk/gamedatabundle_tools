
import os

_base_path = "../PillarsOfEternityII_Data/exported/design/gamedata/"
_export_path = "./export/"
_subpath_bundles = "design/gamedata/"
_subpath_text = "localized/en/text/game/"

def _ensure_dir(path):
    try:  
        os.makedirs(path)
    except OSError:  
        print ("Creation of the directory %s failed" % path)
    else:  
        print ("Successfully created the directory %s" % path)

def ensure_dirs():
    _ensure_dir(_export_path + _subpath_bundles)
    _ensure_dir(_export_path + _subpath_text)

def get(search_string):
    return _base_path + search_string.lower() + ".gamedatabundle"

def get_for_export(base_name):
    return _export_path + _subpath_bundles + base_name.lower() + ".gamedatabundle"

def get_unlock_condition_template_path():
    return "templates/unlock_condition_template.json"

def get_ability_unlock_template_path():
    return "templates/ability_unlock_template.json"

def get_subclass_template_path():
    return "templates/ability_unlock_template.json"

def get_description_file_template_path():
    return "templates/description_file_template.xml"

def get_text_file_export(name):
    return _export_path + _subpath_text + name + ".stringtable"