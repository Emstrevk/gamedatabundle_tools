#!/bin/python

import json
import codecs
import uuid
import difflib

import file_paths
import game_logic_lib

from bundle_keys import BUNDLE_KEY
from bundle_keys import DEBUG_NAME
from bundle_keys import OBJECT_TYPE
from bundle_keys import OBJECT_ID

from collections import OrderedDict

# Read a .gamedatabundle and return the resulting json dict
def _read_bundle(bundle_path):
    with open(bundle_path) as raw_file:
        # Data must be read as ordered and correctly decoded
        # Game expectes $type to be the first json key of any component
        data = json.load(codecs.open(bundle_path, 'r', 'utf-8-sig'), object_pairs_hook=OrderedDict)
        raw_file.close()

    return data

# Find all objects in file with similar DebugName
def find(file_path, name_search_string, absolute=False):

    matches = []

    gamedatabundle = _read_bundle(file_path)
    object_array = gamedatabundle.get(BUNDLE_KEY)

    for object_entry in object_array:
        name = object_entry.get(DEBUG_NAME)
        if (absolute):
            if name_search_string.lower() == name.lower():
                matches.append(object_entry)
        else:
            if name_search_string.lower() in name.lower():
                matches.append(object_entry)

    return matches

def _get_by_id(file_path, id_string):
    matches = []

    gamedatabundle = _read_bundle(file_path)
    object_array = gamedatabundle.get(BUNDLE_KEY)

    for object_entry in object_array:
        id = object_entry.get(OBJECT_ID)
        if id_string == id:
            matches.append(object_entry)

    return matches[0]

# Create a file with the objects debugname containing the object wrapped as a bundle
# Generates a random new ID and adds a suffix to the debugname to make it unique
def _export_object(object, override_game_default=False):

    # Create a new id and name to differentiate if not overriding
    if not override_game_default:
        object[OBJECT_ID] = str(uuid.uuid4())
        if ("Exported" not in object[DEBUG_NAME]):
            object[DEBUG_NAME] = object[DEBUG_NAME] + "_Exported"

    bundle = _get_object_as_bundle(object)
    file_name = file_paths.get_for_export(object.get(DEBUG_NAME))
    _write_json_to_file(bundle, file_name)

    return file_name

def _extract_object_from_bundle(bundle):
    return bundle[BUNDLE_KEY][0]

def _export_bundle(bundle):
    object = _extract_object_from_bundle(bundle)

    return _export_object(object)

def _write_json_to_file(json_object, path):
    with open(path, "wb") as newFile:
        object_string = json.dumps(json_object, indent=4)
        # TODO: Not sure if this is actually an issue
        # Trying it since my mods plain won't load
        # 
        object_string = codecs.encode(object_string, encoding='utf-8-sig')
        newFile.write(object_string)
        newFile.close()

# Wrap an object array in the gamedataobjects key dict to be ready for print to file
def _get_array_as_bundle(object_array):

    full_dict = {}
    full_dict[BUNDLE_KEY] = object_array

    return full_dict

def _get_object_as_bundle(object):

    array = [object]

    return _get_array_as_bundle(array)

def get_class_object(class_name):
    class_file = file_paths.get("characters")
    matches = find(class_file, class_name, True)
    for match in matches:
        if match[OBJECT_TYPE] == "Game.GameData.CharacterClassGameData, Assembly-CSharp":
            return match

    print("ERROR: Class not found: " + class_name)

# Fetch closest match to search string without arguing about ids
def lazy_get_ability_object(ability_name):
    initial_matches = find(file_paths.get("abilities"), ability_name)
    abilities_only = []
    debug_names = []

    # Filter out non-abilities
    for match in initial_matches:
        if match[OBJECT_TYPE] == "Game.GameData.GenericAbilityGameData, Assembly-CSharp":
            abilities_only.append(match)
            debug_names.append(match[DEBUG_NAME])

    close_match_names = difflib.get_close_matches(ability_name, debug_names)

    if (len(close_match_names) == 0):
        
        return abilities_only[0]

    for ability in abilities_only:
        if ability[DEBUG_NAME] == close_match_names[0]:
            return ability
    
    print("Warning: No close matches for " + ability_name)
    return abilities_only[0]

def get_subclass_object(class_name, subclass_name):
    class_file = file_paths.get("characters")
    full_name = class_name + "_" + subclass_name
    matches = find(class_file, full_name, True)

    if (class_name.lower() == "priest"):
        type_string = "Game.GameData.PriestSubClassGameData, Assembly-CSharp"
    else:
        type_string = "Game.GameData.CharacterSubClassGameData, Assembly-CSharp"

    for match in matches:
        if match[OBJECT_TYPE] == type_string:
            return match 

    print("ERROR: subclass not found: " + full_name)

def change_ability_to_class(ability_object, class_object, custom_usage_value=None, custom_usage_type=None):

    ability_object["Components"][0]["AbilityClassID"] = class_object[OBJECT_ID]
    ability_object[DEBUG_NAME] = ability_object[DEBUG_NAME] + "_" + class_object[DEBUG_NAME] 

    ability_object = change_ability_upgrades_from(ability_object)

    if (custom_usage_type is None):
        ability_object["Components"][0]["UsageType"] = game_logic_lib.get_usage_type(class_object[DEBUG_NAME])
    else:
        ability_object["Components"][0]["UsageType"] = custom_usage_type

    if (custom_usage_value is not None):
        ability_object["Components"][0]["UsageValue"] = custom_usage_value

    return ability_object

def change_ability_upgrades_from(ability_object, source_ability_id="00000000-0000-0000-0000-000000000000"):
    ability_object["Components"][0]["UpgradedFromID"] = source_ability_id
    return ability_object

def export_progression_table(class_name):
    object = _get_progression_table(class_name)
    return _export_object(object)

def _get_progression_table(class_name):

    debug_name = "pt_" + class_name.lower()
    matches = find(file_paths.get("progressiontables"), debug_name, True)
    # TODO:Do some kind of "if file exists exported get that instead" logic 
    return matches[0]

def create_new_subclass(class_name, new_subclass_name):
    class_object = get_class_object(class_name)
    template_path = file_paths.get_subclass_template_path()
    template_object = _read_bundle(template_path)

    template_object[OBJECT_ID] = str(uuid.uuid1())
    template_object[DEBUG_NAME] = class_name + "_" + new_subclass_name
    template_object["Components"][0]["ForClassId"] = class_object[OBJECT_ID]

    return template_object


def disable_in_pt_for_subclass(pt_file, ability_name, subclass_id):

    table_from_file = _read_bundle(pt_file)
    table_object = _extract_object_from_bundle(table_from_file)

    edited_ability_unlock_array = table_object["Components"][0]["AbilityUnlocks"]

    unlock_condition_template = _read_bundle(file_paths.get_unlock_condition_template_path())
    unlock_condition_template["Data"]["Parameters"][0] = subclass_id
    unlock_condition_template["Not"] = "true"

    for ability_unlock in edited_ability_unlock_array:
        if ability_name.lower() in ability_unlock["Note"].strip().lower():
            ability_unlock_conditions = ability_unlock["Prerequisites"]["Conditional"]["Components"]

            ability_already_disabled = False
            for condition in ability_unlock_conditions:
                if condition["Data"]["Parameters"][0] == subclass_id and condition["Not"] == "true":
                    ability_already_disabled = True

            # This appends the disable-modified condition template, removing the ability for the subclass
            if not ability_already_disabled:
                ability_unlock["Prerequisites"]["Conditional"]["Components"].append(unlock_condition_template)
                ability_unlock["Prerequisites"]["VisibilityConditional"]["Components"].append(unlock_condition_template)

    _export_object(table_object, override_game_default=True)

def assign_description_to_object(object, description_id):
    object["Components"][0]["Description"] = description_id
    return object

def assign_name_to_object(object, name_id):
    object["Components"][0]["DisplayName"] = name_id
    return object

# TODO: None of these modify visibility conditions explicitly
def enable_in_pt_for_subclass(pt_file, ability_name, subclass_id, require_if_none=False):
    table_from_file = _read_bundle(pt_file)
    table_object = _extract_object_from_bundle(table_from_file)

    edited_ability_unlock_array = table_object["Components"][0]["AbilityUnlocks"]

    unlock_condition_template = _read_bundle(file_paths.get_unlock_condition_template_path())
    unlock_condition_template["Data"]["Parameters"][0] = subclass_id
    unlock_condition_template["Not"] = "false"

    for ability_unlock in edited_ability_unlock_array:
        if ability_name.lower() in ability_unlock["Note"].strip().lower():
            ability_unlock_conditions = ability_unlock["Prerequisites"]["Conditional"]["Components"]
            visibility_conditionals = ability_unlock["Prerequisites"]["VisibilityConditional"]["Components"]

            other_subclass_stated_as_required = False

            for condition in ability_unlock_conditions:
                if condition["Data"]["Parameters"][0] == subclass_id and condition["Not"] == "true":
                    ability_unlock["Prerequisites"]["Conditional"]["Components"].remove(condition)

                # If not already explicitly enabled ...
                elif condition["Data"]["Parameters"][0] == subclass_id and condition["Not"] == "false":
                    if condition["Data"]["Parameters"][0] != subclass_id and condition["Not"] == "true":
                        other_subclass_stated_as_required = True

            # DO same for visibility conditionals
            for visibility_con in visibility_conditionals:
                if visibility_con["Data"]["Parameters"][0] == subclass_id and visibility_con["Not"] == "true":
                    ability_unlock["Prerequisites"]["VisibilityConditional"]["Components"].remove(visibility_con)

                # If not already explicitly enabled ...
                elif visibility_con["Data"]["Parameters"][0] == subclass_id and visibility_con["Not"] == "false":
                    if visibility_con["Data"]["Parameters"][0] != subclass_id and visibility_con["Not"] == "true":
                        other_subclass_stated_as_required = True

            # If ability explicitly requires another subclass we need to add ours as well
            if other_subclass_stated_as_required or require_if_none:
                ability_unlock["Prerequisites"]["Conditional"]["Components"].append(unlock_condition_template)
                ability_unlock["Prerequisites"]["VisibilityConditional"]["Components"].append(unlock_condition_template)

    table_object["Components"][0]["AbilityUnlocks"] = edited_ability_unlock_array

    _export_object(table_object, override_game_default=True)


# Grab a previously exported ability file and add that id to a class progression table
# SHould be add for all as base (skipping the conditional); we can deal with enable/disable for subclass in other methods
# Also this should probably just take the objects and not concern itself with the files
def _add_to_progression_table(pt_file, exported_file_path):
    # TODO: For which subclass etc

    table_from_file = _read_bundle(pt_file)
    table_object = _extract_object_from_bundle(table_from_file)

    ability_from_file = _read_bundle(exported_file_path)
    ability_object = _extract_object_from_bundle(ability_from_file)

    # Templates come without wrapping
    new_unlock_template = _read_bundle(file_paths.get_ability_unlock_template_path())

    new_unlock_template["Note"] = ability_object[DEBUG_NAME]
    new_unlock_template["AddAbilityID"] = ability_object[OBJECT_ID]

    # Get the class id for the unlock requirement from the ability object
    new_unlock_template["Prerequisites"]["PowerLevelRequirement"]["ClassID"] = \
        ability_object["Components"][0]["AbilityClassID"]

    # Get power level requirement from ability level (This should probably be modular)
    new_unlock_template["Prerequisites"]["PowerLevelRequirement"]["MinimumPowerLevel"] = \
        ability_object["Components"][0]["AbilityLevel"]

    # Add the new unlock to the PT
    table_object["Components"][0]["AbilityUnlocks"].append(new_unlock_template)

    _export_object(table_object, override_game_default=True)

# Next up: Descriptions
# Best way is probably to maintain a description object that you then write to file
# ds = new DescriptionFile()
# describeAbility(ability, ds.newEntry("A real shitshow of a spell."))